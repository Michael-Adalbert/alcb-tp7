#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/types.h>
#include <linux/uaccess.h>
#include <linux/fs.h>
#include <linux/list.h>
#include <linux/sched.h>

#define LICENCE "GPL"
#define AUTEUR "Michael Adalbert michael.adalbert@univ-tlse3.fr"
#define DESCRIPTION "Exemple de module Master CAMSI"
#define DEVICE "device_tp6"

#define MIN(a, b) ((a) < (b)) ? (a) : (b)

int my_open(struct inode *i, struct file *f);
int my_release(struct inode *i, struct file *f);

//---------------------------------------------------------------

typedef struct
{
    int associate_pid;
    struct list_head list;
    struct list_head buffer;
} buffer_node_t;

struct list_head ma_list;

//----------------------------------------------------------------
/**
 *  Buffer  
 *  Noeud de buffer
 * */
typedef struct
{
    char *data;
    int size, cursor;
    struct list_head list;
} data_node_t;

struct list_head *current_list = NULL;

int init_flag = 0;

//---------------------------------------------------------------------

static dev_t dev;

static struct cdev *my_cdev;

static struct file_operations my_fops = {
    .owner = THIS_MODULE,
    .open = my_open,
    .release = my_release,
};

static struct class *cl = NULL;

data_node_t *current_dnt_read = NULL;
int current_data_pos = 0;

//---------------------------------------------------------------

static void list_destroy(void)
{
    buffer_node_t *pos, *next;
    data_node_t *in_pos, *in_next;
    printk(KERN_ALERT "[ Driver action ] free data buffer state \n");
    list_for_each_entry_safe(pos, next, &ma_list, list)
    {
        list_for_each_entry_safe(in_pos, in_next, &pos->buffer, list)
        {
            list_del(&in_pos->list);
            kfree(in_pos->data);
            kfree(in_pos);
        }
        list_del(&pos->list);
        kfree(pos);
    }
}

//----------------------------------------------------------------

ssize_t my_read(struct file *f, char *buf, size_t s, loff_t *t)
{
    data_node_t *dnt;
    int size_to_copy, data_in_node, rr = 0;
    printk(KERN_ALERT "[ Driver action ] read of %ld octet \n", s);
    if (s > 0 && !list_empty(current_list) && current_dnt_read != NULL)
    {
        dnt = current_dnt_read;
        data_in_node = dnt->size - current_data_pos;
        size_to_copy = MIN(s, data_in_node);
        if ((rr = copy_to_user(buf, dnt->data + current_data_pos, size_to_copy)) == 0)
        {
            if (size_to_copy == data_in_node)
            {
                if ((dnt->list.next) == current_list)
                {
                    current_dnt_read = NULL;
                    current_data_pos = 0;
                }
                else
                {
                    current_dnt_read = list_first_entry(&dnt->list, data_node_t, list);
                    current_data_pos = 0;
                }
            }
            else
            {
                current_data_pos = current_data_pos + size_to_copy;
            }
            printk(KERN_ALERT "[ Driver action ] read of %d octet\n", size_to_copy);
            return size_to_copy;
        }
        else
        {
            return -EFAULT;
        }
    }
    return 0;
}

ssize_t my_read_destroy(struct file *f, char *buf, size_t s, loff_t *t)
{
    data_node_t *dnt;
    int size_to_copy, data_in_node, rr = 0;
    printk(KERN_ALERT "[ Driver action ] read of %ld octet \n", s);
    if (s > 0 && !list_empty(current_list) && current_dnt_read != NULL)
    {
        dnt = current_dnt_read;
        data_in_node = dnt->size - current_data_pos;
        size_to_copy = MIN(s, data_in_node);
        if ((rr = copy_to_user(buf, dnt->data + current_data_pos, size_to_copy)) == 0)
        {
            dnt->cursor = dnt->cursor + size_to_copy;
            if (size_to_copy == data_in_node)
            {
                if ((dnt->list.next) == current_list)
                {
                    current_dnt_read = NULL;
                    current_data_pos = 0;
                }
                else
                {
                    current_dnt_read = list_first_entry(&dnt->list, data_node_t, list);
                    current_data_pos = 0;
                }
                list_del(&dnt->list);
                kfree(dnt->data);
                kfree(dnt);
            }
            else
            {
                current_data_pos = dnt->cursor;
            }
            printk(KERN_ALERT "[ Driver action ] read of %d octet\n", size_to_copy);
            return size_to_copy;
        }
        else
        {
            return -EFAULT;
        }
    }
    return 0;
}

ssize_t my_write(struct file *f, const char *buf, size_t s, loff_t *t)
{
    printk(KERN_ALERT "[ Driver action ] write of %ld octet \n", s);
    if (s > 0)
    {
        data_node_t *dnt = kmalloc(sizeof(data_node_t), GFP_KERNEL);
        if (dnt)
        {
            dnt->data = kmalloc(sizeof(char) * s, GFP_KERNEL);
            if (dnt->data)
            {
                if (copy_from_user(dnt->data, buf, s) == 0)
                {
                    dnt->size = s;
                    dnt->cursor = 0;
                    INIT_LIST_HEAD(&(dnt->list));
                    list_add_tail(&(dnt->list), current_list);
                    return dnt->size;
                }
            }
            kfree(dnt);
        }
        return -EINVAL;
    }
    return s;
}

struct list_head *get_pid_list(int pid)
{
    buffer_node_t *node;
    struct list_head *pid_list;
    pid_list = NULL;
    list_for_each_entry(node, &ma_list, list)
    {
        if (node->associate_pid == pid)
        {
            pid_list = &node->buffer;
        }
    }
    return pid_list;
}

struct list_head *alloc_pid_list(int pid)
{
    buffer_node_t *bnt;
    bnt = kmalloc(sizeof(buffer_node_t), GFP_KERNEL);
    if (bnt)
    {

        bnt->associate_pid = pid;
        INIT_LIST_HEAD(&(bnt->list));
        INIT_LIST_HEAD(&(bnt->buffer));
        list_add_tail(&(bnt->list), &ma_list);
        return &bnt->buffer;
    }
    else
        return NULL;
}

int my_open(struct inode *i, struct file *f)
{
    dev_t current_dev;
    int pid;
    struct list_head *cr_l;

    current_dev = i->i_rdev;

    pid = current->pid;

    printk(KERN_ALERT "[CALL PID ] : %d \n", pid);

    cr_l = get_pid_list(pid);

    if (cr_l)
    {
        current_list = cr_l;
    }
    else
    {
        current_list = alloc_pid_list(pid);
    }

    if ((f->f_flags & O_ACCMODE) == O_WRONLY && MINOR(current_dev) == MINOR(dev))
    {
        my_fops.write = my_write;
    }
    else if ((MINOR(current_dev) == MINOR(dev) + 2 || MINOR(current_dev) == MINOR(dev) + 1) && (f->f_flags & O_ACCMODE) == O_RDONLY)
    {
        if (current_dnt_read == NULL && !list_empty(current_list))
            current_dnt_read = list_first_entry(current_list, data_node_t, list);
        if (MINOR(current_dev) == MINOR(dev) + 2)
        {
            my_fops.read = my_read_destroy;
        }
        else
        {
            my_fops.read = my_read;
        }
    }
    return 0;
}

int my_release(struct inode *i, struct file *f)
{
    my_fops.write = NULL;
    my_fops.read = NULL;
    return 0;
}

//----------------------------------------------------------------------

static int buff_init(void)
{
    if (alloc_chrdev_region(&dev, 0, 3, DEVICE) == -1)
    {
        printk(KERN_ALERT ">>> ERROR alloc_chrdev_region\n");
        return -EINVAL;
    }
    printk(KERN_ALERT "[ Init allocated ] (major, minor)=(%d,%d)\n", MAJOR(dev), MINOR(dev));
    printk(KERN_ALERT "[ Driver State ] starting\n");

    my_cdev = cdev_alloc();
    my_cdev->ops = &my_fops;
    my_cdev->owner = THIS_MODULE;

    cl = class_create(THIS_MODULE, "chrdev");

    if (cl == NULL)
    {
        printk(KERN_ALERT ">>> ERROR : Class creation failed\n");
        unregister_chrdev_region(dev, 1);
        return -EINVAL;
    }

    if (device_create(cl, NULL, MKDEV(MAJOR(dev), MINOR(dev) + 0), NULL, "sample-write") == NULL)
    {
        printk(KERN_ALERT ">>> ERROR : Device creation failed\n");
        class_destroy(cl);
        unregister_chrdev_region(MKDEV(MAJOR(dev), 0), 1);
        return -EINVAL;
    }

    if (device_create(cl, NULL, MKDEV(MAJOR(dev), MINOR(dev) + 1), NULL, "sample-read") == NULL)
    {
        printk(KERN_ALERT ">>> ERROR : Device creation failed\n");
        class_destroy(cl);
        unregister_chrdev_region(MKDEV(MAJOR(dev), 0), 2);
        return -EINVAL;
    }

    if (device_create(cl, NULL, MKDEV(MAJOR(dev), MINOR(dev) + 2), NULL, "sample-read-destroy") == NULL)
    {
        printk(KERN_ALERT ">>> ERROR : Device creation failed\n");
        class_destroy(cl);
        unregister_chrdev_region(MKDEV(MAJOR(dev), 0), 3);
        return -EINVAL;
    }

    if (cdev_add(my_cdev, MKDEV(MAJOR(dev), 0), 3) <= -1)
    {
        device_destroy(cl, dev);
        class_destroy(cl);
        unregister_chrdev_region(MKDEV(MAJOR(dev), 0), 3);
        return -EINVAL;
    }

    INIT_LIST_HEAD(&ma_list);

    return 0;
}

static void buff_cleanup(void)
{
    printk(KERN_ALERT "[ Driver State ] stopping\n");
    cdev_del(my_cdev);
    device_destroy(cl, MKDEV(MAJOR(dev), MINOR(dev) + 0));
    device_destroy(cl, MKDEV(MAJOR(dev), MINOR(dev) + 1));
    device_destroy(cl, MKDEV(MAJOR(dev), MINOR(dev) + 2));
    class_destroy(cl);
    unregister_chrdev_region(MKDEV(MAJOR(dev), 0), 3);
    list_destroy();
}

MODULE_LICENSE(LICENCE);
MODULE_AUTHOR(AUTEUR);
MODULE_DESCRIPTION(DESCRIPTION);
MODULE_SUPPORTED_DEVICE(DEVICE);

module_init(buff_init);
module_exit(buff_cleanup);
