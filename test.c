#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

#define SIZE_BUFF 20

char buff[SIZE_BUFF];

char *message = "Hello World !";

void write_my_data()
{
    int f;
    if ((f = open("/dev/sample-write", O_WRONLY)) == -1)
    {
        printf("cannot open the file");
        exit(1);
    }

    if (write(f, message, 14) > 0)
    {
        fprintf(stdout, "write ok\n");
    }
    else
    {
        fprintf(stdout, "write not ok\n");
    }
    close(f);
}

void read_my_data()
{
    int f;
    if ((f = open("/dev/sample-read", O_RDONLY)) == -1)
    {
        printf("cannot open the file");
        exit(1);
    }
    int n_byte;
    while ((n_byte = read(f, buff, SIZE_BUFF)) > 0)
    {
        fprintf(stdout, "Lu (%d) : %s\n", n_byte, buff);
    }

    close(f);
}

int main(int argc, char **argv)
{
    write_my_data();
    write_my_data();
    write_my_data();
    read_my_data();
    read_my_data();
    return 0;
}